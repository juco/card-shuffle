describe 'Home page' do
  before :each do
    visit home_url
  end

  it 'should load successfully' do
    expect(page.status_code).to be 200
  end

  it 'should have the correct header' do
    expect(page.find('h1').text).to have_content "shufflin'"
  end

  it 'should display all the cards' do
    expect(page.all('.card').count).to be 52
  end

  it 'should load in an ordered state' do
    card_elements = page.all('.card')
    cards = Cards::Deck.new.cards
    expect(card_elements[0].find('.top .number').text).to eq cards[0].number
    expect(card_elements[6].find('.top .number').text).to eq cards[6].number
  end

  it 'should have a button to allow for shuffling' do
    expect(page).to have_content('Shuffle')
  end

  it 'should have a working shuffle button' do
    click_link 'Shuffle'
    card_elements = page.all('.card')
    cards = Cards::Deck.new.cards
    expect(card_elements[0].find('.top .number').text).not_to eq cards[0].number
    expect(card_elements[6].find('.top .number').text).not_to eq cards[6].number
  end
end
