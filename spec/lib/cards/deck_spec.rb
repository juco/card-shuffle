require 'spec_helper'
require 'cards/deck'

describe 'Card Deck' do
  let(:expected_number_count) { 13 }
  let(:expected_card_count) { 52 }
  let(:ordered_start) {[
    Cards::Card.new(:spades, '2'),
    Cards::Card.new(:spades, '3'),
    Cards::Card.new(:spades, '4')
  ]}

  it 'should have the correct number of available cards' do
    expect(Cards::Deck::NUMBERS.count).to eq expected_number_count
  end

  it 'should have the correct number of suits' do
    expect(Cards::Deck::SUITS.count).to eq 4
  end

  it 'should initialize with the correct number of cards' do
    expect(Cards::Deck.new.cards.count).to eq expected_card_count
  end

  it 'should initialize in the correct order' do
    cards = Cards::Deck.new.cards
    expect(cards[0..2]).to match_array ordered_start
  end

  it 'should allow for randomising' do
    shuffled = Cards::Deck.new.shuffle
    expect(shuffled[0..2]).not_to eq ordered_start
  end
end
