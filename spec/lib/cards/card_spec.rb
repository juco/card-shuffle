require 'rspec'
require 'cards/card'

describe 'Cards Card' do
  it 'should initialize with the correct values' do
    card = Cards::Card.new(:heart, 'Q')
    expect(card.suit).to eq :heart
    expect(card.number).to eq 'Q'
  end
end
