# Card Shuffling

A quick card shuffling web app.

I doubt it's the prettiest example you've seen, but I thought you would rather
emphasis on the technologies, so I have included some sample Unit and Integration
tests.

## Instructions
Instructions to run and test the app

### Run
After cloning:

* `bundle install`
* `rails server`

### Development
* Run the tests with `bundle exec rspec`
* Guard the test files and live reload with `bundle exec guard`

# Author
[julian@juco.co.uk](http://www.juco.co.uk)
