class HomeController < ApplicationController
  def show
    deck = Cards::Deck.new
    deck.shuffle if params[:shuffle]
    @cards = deck.cards
  end
end
