module Cards
  class Card
    attr_reader :suit, :number

    def initialize(suit, number)
      @suit = suit
      @number = number
    end

    def ==(other)
      other.number == number and other.suit == suit
    end
  end
end
