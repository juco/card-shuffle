module Cards
  class Deck
    NUMBERS = (2..10).to_a.map(&:to_s) | ['J', 'Q', 'K', 'A']
    SUITS = [:spades, :clubs, :hearts, :diamonds]

    def cards
      @cards ||= SUITS.flat_map do |suit|
        NUMBERS.map do |number|
          Card.new(suit, number)
        end
      end
    end

    def shuffle
      @cards = cards.shuffle
    end
  end
end
